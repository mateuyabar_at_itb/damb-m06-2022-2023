package cat.itb.mateuyabar.dam.m06.uf1.bicingSample

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class BicingRepositoryTest {

    @Test
    fun listStations() {
        val list = BicingRepository().listStations()
        assert(list.isNotEmpty())
    }
}