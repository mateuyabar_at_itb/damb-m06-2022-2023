package cat.itb.mateuyabar.dam.m06.uf1

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.ContentType.Application.Json
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.net.http.HttpClient



@Serializable
data class BcnTree(
    @SerialName("nom_cientific") val scientificName : String,
    @SerialName("nom_catala") val name: String?,
    @SerialName("adreca") val adress: String
)

suspend fun main(){
    val client = HttpClient(CIO){
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val trees : List<BcnTree> = client.get("https://opendata-ajuntament.barcelona.cat/resources/bcn/Arbrat/OD_Arbrat_Zona_BCN.json").body()
    println("There are ${trees.size} in Barcelona")
    println("Example ${trees.first()}")
}