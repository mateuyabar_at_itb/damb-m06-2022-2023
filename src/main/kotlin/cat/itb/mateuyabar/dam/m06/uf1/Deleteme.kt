package cat.itb.mateuyabar.dam.m06.uf1

import nl.adaptivity.xmlutil.serialization.XML

@kotlinx.serialization.Serializable
data class Person(val name: String, val age: Int)
fun main() {
    val person = Person("Mila", 55)
    val xmlPerson = XML.encodeToString(person)
    println(xmlPerson)


}