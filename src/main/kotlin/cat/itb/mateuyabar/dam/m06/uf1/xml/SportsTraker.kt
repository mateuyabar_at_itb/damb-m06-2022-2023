package cat.itb.mateuyabar.dam.m06.uf1.xml

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import java.nio.file.Path
import java.util.Scanner
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

@Serializable
data class SportLog(val sport: String, val seconds : Int)

val path = Path.of("sportslog.xml")

fun main() {
    val scanner = Scanner(System.`in`)
    val log = readLog()
    val newLog = addNewSport(scanner, log)
    printResumee(newLog)
    storeLog(newLog)
}

fun printResumee(log: List<SportLog>) {
    log.groupBy { it.sport }
        .mapValues {
            it.value.sumOf { it.seconds }
        }
        .forEach{
            println("${it.key} - ${it.value}")
        }
}

fun storeLog(newLog: List<SportLog>) {
    val xml = XML.encodeToString(newLog)
    path.writeText(xml)
}

fun addNewSport(scanner: Scanner, log: List<SportLog>): List<SportLog> {
    val sport = scanner.nextLine()
    val time = scanner.nextInt()
    val sportLog = SportLog(sport, time)
    return log+sportLog
}

fun readLog(): List<SportLog> {
    return if(path.exists()){
        val xml = path.readText()
        XML.decodeFromString(xml)
    } else {
        listOf()
    }
}
