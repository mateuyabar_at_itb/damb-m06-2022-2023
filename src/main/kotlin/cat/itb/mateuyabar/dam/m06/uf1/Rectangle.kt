package cat.itb.mateuyabar.dam.m06.uf1

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
data class Rectangle(
    @SerialName("w") val width: Double,
    val height: Double)

fun main() {
    val rec = Rectangle(2.2, 3.5)
    val json = Json.encodeToString(rec)
    val rec2 : Rectangle = Json.decodeFromString(json)
    println(rec2)
}