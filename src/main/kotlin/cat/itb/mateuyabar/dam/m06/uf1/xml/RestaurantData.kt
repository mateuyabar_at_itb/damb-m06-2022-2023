package cat.itb.mateuyabar.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement

@Serializable
@SerialName("restaurant")
data class Restaurant(
    val type: String,
    @XmlElement(true) val name: String,
    @XmlElement(true) val address: String,
    @XmlElement(true) val owner: String
)

fun main() {
    val r = Restaurant("oij", "oij", "oij", "oiu")
    println(XML.encodeToString(r))

    val xml = Restaurant::class.java.getResource("/restaurant.xml").readText()
    val restaurant : Restaurant = XML.Companion.decodeFromString(xml)
    printRestaurant(restaurant)
}

fun printRestaurant(restaurant: Restaurant) {
    println("${restaurant.name} - ${restaurant.address} - ${restaurant.owner}")
}
