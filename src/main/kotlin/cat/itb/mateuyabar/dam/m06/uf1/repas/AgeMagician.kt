package cat.itb.mateuyabar.dam.m06.uf1.repas

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.util.*

@Serializable
data class AgifyData(val age: Int)

suspend fun main() {
    val scanner = Scanner(System.`in`)
    println("Ben vingut al AgeMagician. Com et dius?")
    val name = scanner.nextLine()
    val age = calculateAgeFor(name)
    println("Jo crec que tens $age anys!")


}

suspend fun calculateAgeFor(name: String): Int {
    val client = HttpClient(CIO){
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val data : AgifyData = client
        .get("https://api.agify.io/?name=$name&country_id=ES")
        .body()
    return data.age
}
