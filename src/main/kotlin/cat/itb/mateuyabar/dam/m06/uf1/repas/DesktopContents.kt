package cat.itb.mateuyabar.dam.m06.uf1.repas

import java.nio.file.Path
import kotlin.io.path.createFile
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.notExists
import kotlin.io.path.writeText

val home = System.getProperty("user.home")

fun main() {
    val count = countFilesInDesktop()
    saveToFile(count)
}

fun saveToFile(count: Int) {
    val path = Path.of(home, "desktopContents.txt")
    path.writeText(count.toString())
}

fun countFilesInDesktop(): Int {
    val desktopPath = Path.of(home, "Escriptori")
    return desktopPath.listDirectoryEntries().size
}
