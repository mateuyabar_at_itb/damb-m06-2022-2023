package cat.itb.mateuyabar.dam.m06.uf2.exercicis

import cat.itb.mateuyabar.dam.m06.uf2.exercicis.Users.name
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

object Users : Table() {
    val id = varchar("id", 10) // Column<String>
    val name = varchar("name", length = 50) // Column<String>
    override val primaryKey = PrimaryKey(id, name = "PK_User_ID") // name is optional here
}

fun main() {
    val db = Database.connect("jdbc:h2:./ex1.db", "org.h2.Driver")
    transaction {
        SchemaUtils.create (Users)

        Users.insert {
            it[id] = "marta"
            it[name] = "Marta Peris"
        }

        Users.selectAll().forEach {
            println(it[Users.name]+" "+it[Users.id])
        }

        val result  = Users
            .select { Users.id eq "marta" }.single()
        println(result[Users.name])
    }
}