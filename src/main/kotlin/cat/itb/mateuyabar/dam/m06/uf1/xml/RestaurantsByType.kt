package cat.itb.mateuyabar.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import java.util.Scanner

@Serializable
@SerialName("restaurants")
data class Restaurants(val restaurants: List<Restaurant>)

fun main() {
    val scanner = Scanner(System.`in`)
    val type = scanner.nextLine()

    val xml = Restaurant::class.java.getResource("/restaurants.xml").readText()
    val restaurants : Restaurants = XML.Companion.decodeFromString(xml)
    val restaurantByType = restaurants.restaurants.filter { it.type == type }
    restaurantByType.forEach{ printRestaurant(it) }

}