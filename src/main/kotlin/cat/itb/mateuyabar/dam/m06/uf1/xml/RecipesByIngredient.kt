package cat.itb.mateuyabar.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlChildrenName
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlValue
import java.util.Scanner


@Serializable
@SerialName("ingredient")
data class Ingredient(
    val ammount: Int,
    val unit: String,
    val name: String
)

@Serializable
@SerialName("recipe")
data class Recipe(
    val dificulty: Int,
    @XmlElement(true) val name: String,
    @XmlChildrenName("ingredient", "", "")
    @SerialName("ingredients")
    val ingredients: List<Ingredient>
)


@Serializable
@SerialName("recipes")
data class Recipes(
    @SerialName("recipe")
    val recipes: List<Recipe>
)

fun main() {
    val recipes = readRecies()
    val scanner = Scanner(System.`in`)
    println("Tell me an ingredient")
    val ingredient = scanner.nextLine()
    recipes.recipes.filter { recipe ->
        recipe.ingredients.any { it.name == ingredient}
    }.sortedBy { it.dificulty }
        .forEach{
            println("$it.name")
        }
}

fun printAsXml(recipes: Recipes) {
    println(XML.encodeToString(recipes))
}

fun readRecies(): Recipes {
    val xml = Recipe::class.java.getResource("/receptes.xml")
        .readText()
    return XML.Companion.decodeFromString(xml)
}
